Source: firmware-microbit-micropython
Section: python
Priority: optional
Maintainer: Debian QA Group <packages@qa.debian.org>
Build-Depends:
 cmake,
 debhelper-compat (= 13),
 dh-python,
 gcc-arm-none-eabi,
 libnewlib-arm-none-eabi,
 libssl-dev,
 libstdc++-arm-none-eabi-newlib,
 ninja-build,
 python3-setuptools,
 python3-sphinx,
 python3-sphinx-rtd-theme,
 srecord,
 yotta,
Standards-Version: 4.6.2
Homepage: https://github.com/bbcmicrobit/micropython
Vcs-Browser: https://salsa.debian.org/python-team/packages/firmware-microbit-micropython
Vcs-Git: https://salsa.debian.org/python-team/packages/firmware-microbit-micropython.git
Rules-Requires-Root: no

Package: firmware-microbit-micropython
Architecture: all
Multi-Arch: foreign
Depends:
 ${misc:Depends},
Suggests:
 firmware-microbit-micropython-doc,
 picocom | screen,
Enhances:
 mu-editor,
 python3-uflash,
Conflicts:
 firmware-microbit-micropython-dl,
Description: MicroPython runtime for the BBC micro:bit
 This package provides a binary firmware file for the  BBC micro:bit
 small board computer (SBC), containing the MicroPython runtime.
 .
 It is suggested that a dedicated flashing tool (e.g. uflash) is used to
 upload the firmware and Python scripts to the micro:bit device.
 .
 The uflash utility (package: python3-uflash) uses the MicroPython runtime
 provided in this package as the default firmware to flash to the micro:bit.
 .
 This package contains the MicroPython runtime for the BBC micro:bit

Package: firmware-microbit-micropython-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Depends:
 ${misc:Depends},
 ${sphinxdoc:Depends},
Suggests:
 www-browser,
Description: MicroPython runtime for the BBC micro:bit (documentation)
 This package provides a binary firmware file for the  BBC micro:bit
 small board computer (SBC), containing the MicroPython runtime.
 .
 It is suggested that a dedicated flashing tool (e.g. uflash) is used to
 upload the firmware and Python scripts to the micro:bit device.
 .
 The uflash utility (package: python3-uflash) uses the MicroPython runtime
 provided in this package as the default firmware to flash to the micro:bit.
 .
 This package contains documentation for MicroPython on the BBC micro:bit
