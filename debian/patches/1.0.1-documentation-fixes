Description: Upstream documentation fixes post 1.0.1 release
Origin: upstream, https://github.com/bbcmicrobit/micropython/compare/v1.0.1...a92ca9b
Reviewed-by: Nick Morrott <nickm@debian.org>
Last-Update: 2020-06-17
---
--- a/docs/audio.rst
+++ b/docs/audio.rst
@@ -42,7 +42,7 @@
 ===========
 
 You will need a sound source, as input to the ``play`` function. You can generate your own, like in
-``examples/waveforms.py`` or you can use the sound sources provided by modules like ``synth``.
+``examples/waveforms.py``.
 
 
 Technical Details
@@ -52,7 +52,7 @@
     You don't need to understand this section to use the ``audio`` module.
     It is just here in case you wanted to know how it works.
 
-The ``audio`` module consumes samples at 7812.5 kHz, and uses linear interpolation to
+The ``audio`` module consumes samples at 7812.5 Hz, and uses linear interpolation to
 output a PWM signal at 32.5 kHz, which gives tolerable sound quality.
 
 The function ``play`` fully copies all data from each ``AudioFrame`` before it
--- a/docs/devguide/repl.rst
+++ b/docs/devguide/repl.rst
@@ -78,16 +78,29 @@
 
 **Mac OS**
 
-Open Terminal and type ``screen /dev/cu.usbmodem1422 115200``, replacing 
-``/dev/cu.usbmodem1422`` with the port you found earlier. This will open the 
-micro:bit's serial output and show all messages received from the device. To 
-exit, press Ctrl-A then Ctrl-D.
+Open Terminal and type ``screen /dev/cu.usbmodem1422 115200``, replacing
+``/dev/cu.usbmodem1422`` with the port you found earlier. This will open the
+micro:bit's serial output and show all messages received from the device.
+
+To exit, press Ctrl-A then Ctrl-\\ and answer Yes to the question. There are
+many ways back to a command prompt including Ctrl-A then Ctrl-D, which will
+detach screen, but the serial port with still be locked, preventing other
+applications from accessing it. You can then restart screen by typing
+``screen -r``.
 
 
 **Linux**
 
-Using the ``screen`` program, type ``screen /dev/ttyUSB0 115200``, replacing 
+Using the ``screen`` program, type ``screen /dev/ttyUSB0 115200``, replacing
 ``/dev/ttyUSB0`` with the port you found earlier.
 
-Using ``picocom``, type ``picocom /dev/ttyACM0 -b 115200``, again replacing 
-``/dev/ttyACM0`` with the port you found earlier. 
+To exit, press Ctrl-A then \\ and answer Yes to the question. There are many
+ways back to a command prompt including Ctrl-A then Ctrl-D, which will detach
+screen. All serial output from the micro:bit will still be received by
+``screen``, the serial port will be locked, preventing other applications from
+accessing it. You can restart screen by typing ``screen -r``.
+
+Using ``picocom``, type ``picocom /dev/ttyACM0 -b 115200``, again replacing
+``/dev/ttyACM0`` with the port you found earlier.
+
+To exit, press Ctrl-A then Ctrl-Q.
--- a/docs/index.rst
+++ b/docs/index.rst
@@ -20,9 +20,6 @@
 
 .. image:: comic.png
 
-To get involved with the community subscribe to the microbit@python.org
-mailing list (https://mail.python.org/mailman/listinfo/microbit).
-
 .. note::
 
     This project is under active development. Please help other
--- a/docs/pin.rst
+++ b/docs/pin.rst
@@ -43,13 +43,13 @@
 +-----+---------+----------+
 |  5  | Digital | Button A |
 +-----+---------+----------+
-|  6  | Digital | Row 2    |
+|  6  | Digital | Column 9 |
 +-----+---------+----------+
-|  7  | Digital | Row 1    |
+|  7  | Digital | Column 8 |
 +-----+---------+----------+
 |  8  | Digital |          |
 +-----+---------+----------+
-|  9  | Digital | Row 3    |
+|  9  | Digital | Column 7 |
 +-----+---------+----------+
 |  10 | Analog  | Column 3 |
 +-----+---------+----------+
@@ -57,11 +57,11 @@
 +-----+---------+----------+
 |  12 | Digital |          |
 +-----+---------+----------+
-|  13 | Digital | SPI MOSI |
+|  13 | Digital | SPI SCK  |
 +-----+---------+----------+
 |  14 | Digital | SPI MISO |
 +-----+---------+----------+
-|  15 | Digital | SPI SCK  |
+|  15 | Digital | SPI MOSI |
 +-----+---------+----------+
 |  16 | Digital |          |
 +-----+---------+----------+
--- a/docs/tutorials/gestures.rst
+++ b/docs/tutorials/gestures.rst
@@ -60,7 +60,7 @@
         "Better not tell you now",
         "Cannot predict now",
         "Concentrate and ask again",
-        "Don't count on it"
+        "Don't count on it",
         "My reply is no",
         "My sources say no",
         "Outlook not so good",
--- a/source/microbit/main.cpp
+++ b/source/microbit/main.cpp
@@ -3,7 +3,6 @@
 #include "microbit/memory.h"
 #include "microbit/filesystem.h"
 #include "microbit/microbitdal.h"
-#include "_newlib_version.h"
 #include "MicroBitButton.h"
 
 // Global instances of the mbed/DAL components that we use
--- a/source/microbit/microbitdisplay.cpp
+++ b/source/microbit/microbitdisplay.cpp
@@ -668,7 +668,7 @@
 
 void microbit_display_init(void) {
     //  Set pins as output.
-    nrf_gpio_range_cfg_output(MIN_COLUMN_PIN, MIN_COLUMN_PIN + COLUMN_COUNT + ROW_COUNT);
+    nrf_gpio_range_cfg_output(MIN_COLUMN_PIN, MIN_COLUMN_PIN + COLUMN_COUNT + ROW_COUNT - 1);
 }
 
 }
